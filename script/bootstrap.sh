#!/bin/bash

echo "***** Initial Install *****"

echo "[TASK $((count=1))] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
172.30.0.200 vegeta
EOF

echo "[TASK $((++count))] Disabling SELinux"
setenforce 0
sed -i --follow-symlinks 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/sysconfig/selinux

echo "[TASK $((++count))] Stopping and Disabling firewalld"
systemctl disable firewalld >/dev/null 2>&1
systemctl stop firewalld

echo "[TASK $((++count))] Installing initial packages"
sudo yum install -y vim wget net-tools unzip telnet ntpdate htop git >/dev/null 2>&1
