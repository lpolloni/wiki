#!/bin/bash

echo "***** Vegeta install *****"

#echo "[TASK $((count=1))] yum update"
#yum update -y >/dev/null 2>&1

echo "[TASK $((++count))] docker install"
yum install docker -y >/dev/null 2>&1
systemctl enable docker >/dev/null 2>&1
systemctl start docker >/dev/null 2>&1

#echo "[TASK $((++count)) docker run container vegeta]"
#docker pull jurid/vegeta:1.0
#docker run -it \
#--name vegeta \
#--hostname vegeta \
#-p 80:80 \
#-v /jurid/config/vegeta/vhots/:/etc/apache2/sites-available/ \
#-v /jurid/data/vegeta/www/:/var/www/ \
#/v /jurid/log/vegeta/apache2:/var/log/apache2/
#-d=true \
#debian
